# -*- coding: utf-8 -*-
# RafKac
# 2021_07_30

# na podstawie filmu instruktarzowego z yt
# rozbudowa - dodać pochodne i całki
# rozbudowa - dodać trzy kolumny w inicjalizacji ekranu - estetyka
# rozbudowa - czyszczenie po obliczeniu                                 v
# rozbudowa - obsługa pierwiastka

import tkinter as tk


symbole = ["7", "8", "9", "/", "\u21BA", 'C', "4", "5", "6", "*", "(", ")", "1", "2", "3", "-", "x^2", "\u221A", "0", ",", "%", "+" ]
COLOR = '#f2f4f7'


def inicjalizacjaOkna():
    root = tk.Tk()
    root.configure(bg = COLOR)
    root.geometry('400x400')
    root.title('Kalkulator')

    return root


def inicjalizacjaEkranu(root):
    ekran = [tk.Label(root, bg = "#C0CBCB", width = 55, anchor = "w", borderwidth = 2) for i in range(3)]

    for i in range(len(ekran)):
        ekran[i].grid(row = i, columnspan = 6, ipady = 15, ipadx = 1)

    return ekran


def inicjalizacjaPolaNaDane(root, ekran):
    poleNaDane = tk.Entry(root, borderwidth = 0, highlightcolor = "white", highlightbackground = "white")
    poleNaDane.grid(row = len(ekran), columnspan = 6, ipadx = 136, ipady = 10)

    info = tk.Label(root, bg = "white", width = 55, anchor = 'w', borderwidth = 2)
    info.grid(row = len(ekran) + 1, columnspan = 6, ipady = 15, ipadx = 1)

    return poleNaDane, info


def przyciskKlik(poleNaDane, symbol):
    def f():
        if symbol == "\u21BA":
            bufor = poleNaDane.get()[:-1]
            poleNaDane.delete(0, tk.END)
            poleNaDane.insert(9, bufor)
        elif symbol == "C":
            poleNaDane.delete(0, tk.END)
        else:
            tekst = symbol if symbol != "x^2" else "^2"
            poleNaDane.insert(tk.END, tekst)

    return f


def oblicz(poleNaDane, ekran, info):
    def czyPoprawnyOstatniZnak(tekst):
        i = 1
        while tekst[-i] == ')':
            i += 1
        return tekst[-i].isdigit


    def czyWielokrotneOperatory(tekst):
        for i in range(len(tekst)):
            if not tekst[i].isdigit() and not tekst[i+1].isdigit():
                return True
        return False


    def zamienZnakPotegi(tekst):
        for i in range(len(tekst)):
            if tekst[i]== '^':
                tekst = tekst[ :i ] + "**" + tekst[ i+1 : ]
        return tekst


    def f():
        tekst = poleNaDane.get()

        if not czyPoprawnyOstatniZnak(tekst) or czyWielokrotneOperatory(tekst):
            info['text'] = "Bledne wyrazenie."
        else:
            info['text'] = ''
            for i in range(1, len(ekran)):
                if ekran[i]['text']:
                    ekran[i - 1]['text'] = ekran[i]['text']
            if '^' in tekst:
                wyrazenie = zamienZnakPotegi(tekst)
                ekran[-1]['text'] = tekst + ' = ' + str(eval(wyrazenie))
            else:
                ekran[-1]['text'] = tekst + ' = ' + str(eval(tekst))
        tekst = ""
        poleNaDane.delete(0, tk.END)
    return f


def inicjalizacjaPrzyciskow(root, ekran, info):
    przyciski = [tk.Button(root, text = symbol, bg = COLOR, borderwidth = 0) for symbol in symbole]

    j = len(ekran) + 2
    for i in range(len(przyciski)):
        if i % 6 == 0:
            j += 1
        margin = 21 if len(symbole[i]) == 1 else 10
        przyciski[i].grid(row = j, column = i % 6, ipady = 5, ipadx = margin )
        przyciski[i].configure(command = przyciskKlik(poleNaDane, przyciski[i]['text']))

    znakRownosci = tk.Button(root, text = "=", bg = "#00BFFF", borderwidth = 0, command = oblicz(poleNaDane, ekran, info))
    znakRownosci.grid(row = len(ekran) + 6, columnspan = 2, column = 4, ipady = 5, ipadx = 50)
    return przyciski


if __name__ == "__main__":
    root = inicjalizacjaOkna()
    ekran = inicjalizacjaEkranu(root)
    poleNaDane, info = inicjalizacjaPolaNaDane(root, ekran)
    przyciski = inicjalizacjaPrzyciskow(root, ekran, info)


    root.mainloop()
